# Flysystem Uploads - Wordpress Plugin

The goal of this plugin is to offload media uploads in Wordpress to a
filesystem of choice.

## Architecture

### Building blocks

* https://flysystem.thephpleague.com/v1/docs/
* https://github.com/deliciousbrains/wp-background-processing

### Steps

* Find the relevant hooks to get notified about media uploads, edits and
  deletions.
  * [`wp_handle_upload`](https://developer.wordpress.org/reference/hooks/wp_handle_upload/)
  * [`wp_handle_upload_prefilter`](https://developer.wordpress.org/reference/hooks/wp_handle_upload_prefilter/)
  * [`wp_upload_bits`](https://developer.wordpress.org/reference/hooks/wp_upload_bits/)
  * [`pre_move_uploaded_file`](https://developer.wordpress.org/reference/hooks/pre_move_uploaded_file/)
  * [`wp_delete_file`](https://developer.wordpress.org/reference/hooks/wp_delete_file/)
  * [`wp_update_attachment_metadata`](https://developer.wordpress.org/reference/hooks/wp_update_attachment_metadata/)
  * [`wp_generate_attachment_metadata`](https://developer.wordpress.org/reference/hooks/wp_generate_attachment_metadata/)
  * [`add_attachment`](https://developer.wordpress.org/reference/hooks/add_attachment/)
  * [`delete_attachment`](https://developer.wordpress.org/reference/hooks/delete_attachment/)
  * [`edit_attachment`](https://developer.wordpress.org/reference/hooks/edit_attachment/)
  * Upload flow
    * [`_wp_handle_upload`](https://developer.wordpress.org/reference/functions/_wp_handle_upload/) is called
    * `wp_handle_upload_prefilter` is applied and receives/mutates a 'Reference to a single element of $\_FILES' (an array with file upload data)
    * Some error checks and tests are done (e.g. whether upload dir is writable)
    * `pre_move_uploaded_file` is applied. When `$move_new_file` is set to a non null value, the uploaded file won't be moved to the uploads dir.
    * Regardless of whether the uploaded file is moved or not, the new file is chmodded
    * Finally `wp_handle_upload` is applied and receives/mutates an assoc array with the new file name, url and file type
* Create async actions to mimic file operations on flysystem. This will create a
  one to one file storage sync.
  * Create `$filesystem->write` or `$filesystem->put`
  * Update `$filesystem->update` or `$filesystem->put`
  * Delete `$filesystem->delete`
* Add some post processing options.
  * Delete files from local file system when they're successfully stored on the
    flysystem.
  * Update attachment link in WP after successful writes
* Wire Wordpress hooks to async actions
  * Looks like create/update operations can be handled by the `wp_handle_upload`
    filter and optionally `pre_move_uploaded_file` to prevent writing data
    to the filesystem at all. Also provides an opportunity to rewrite the url.
* Unknowns
  * What happens when generating image thumbnails and file previews? Do we have
    to deal with that too?
    \> What happens is this: after the wp_upload limbo, the 
    wp_generate_attachment_metadata filter is applied in the
    wp_update_image_subsizes function.
  * When editing an image from the media library there's no option to replace
    the image with a different file, but it's possible to edit the image by
    modifying it (crop, flip, etc.). How does that work?

### Test scenario's

* Upload an new file in the media library
* Upload a new file in a post or page
* Edit an image
* Delete a file from the media library

### Approach

Wordpress heavily relies on writing files to the file system for different
operations. Since my first objective is storing uploads at a different location
to save disk space I think it's best to keep the uploads on the file system as
a kind of intermediary cache. Whenever a file is created, updated or deleted
I simply signal the flysystem something is changed. Changes are queued and
get processed in the background. When the changes are applied, metadata (e.g.
file url) in Wordpress is updated.

Files are kept on the filesystem, until a limit is reached. This limit can 
either be based on used disk space or an expiry time. When the disk space limit
is reached or a files cache time is expired the file gets removed. This can
happen in a (periodic) background task.

This calls for the appropriate handling of updating metadata. Lets consider the
following scenario: image is edited and saved, change is queued, image is edited
and saved again before queued change is processed. Then the queued change gets
applied, updating the new metadata and thus temporarily 'reverting' the new
change until that gets processed by the queue. The data will be eventually in
sync when the queue is processed in order, but it might lead to unexpected
results when editing a file.

This also introduces the need to hook into all situations where Wordpress
expects a file to be present on the file system. For example when editing an
image or deleting a file.

## Questions

### What other plugins are available to accomplish the same goal?

* WP Offload Media Lite - https://wordpress.org/plugins/amazon-s3-and-cloudfront/
* Media Cloud - https://wordpress.org/plugins/ilab-media-tools/

### There are already plugins which make this possible. Why write another one?

As a developer I'd answer:

* Those plugins are not written by me
* Only god knows what kind of back doors I pull in
* I'd rather write my own code than review that of someone else
* The popular plugins are really bloated, I can write something more efficient
* I've got so many good ideas to architect a beautiful solution (even despite 
  this being a Wordpress plugin)!
* Existing plugins are made for Wordpress site administrators. I would like to
  make something that's more oriented towards developers.

As an open source enthusiast (and freerider) I'd answer:

* I don't like the freemium model of the popular plugins
* With the existing plugins you're bound to the storage providers supported by 
  the plugin authors. When allowing the registration of other flysystem adapters
  you'll open up the potential to connect any storage you'd like.
