<?php
/**
 * Plugin Name:     FlySystem
 * Plugin URI:      {{plugin_uri}}
 * Description:     Offload media uploads to a file system of choice
 * Author:          Skript Creative Coding
 * Author URI:      https://skript.cc
 * Text Domain:     skript_flysystem
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Skript\Wpp\FlySystem
 */

namespace Skript\Wpp\FlySystem;

function run()
{
    return true;
}

function on_activate() {
    // stub
}

function on_deactivate() {
    // stub
}

function on_uninstall() {
    // stub
}

register_activation_hook(__FILE__, __NAMESPACE__.'\on_activate');
register_deactivation_hook(__FILE__, __NAMESPACE__.'\on_deactivate');
register_uninstall_hook(__FILE__, __NAMESPACE__.'\on_uninstall');