<?php

/**
 * Map lando's environment info to WP_ env vars
 */
$lando = json_decode(getenv('LANDO_INFO') ?: '{}', true);

// database settings
if (isset($lando['database'])) {
    $db=$lando['database'];
    putenv('WP_DB_NAME='.$db['creds']['database']);
    putenv('WP_DB_USER='.$db['creds']['user']);
    putenv('WP_DB_PASSWORD='.$db['creds']['password']);
    putenv(
        'WP_DB_HOST='
        . $db['internal_connection']['host']
        . ':'
        . $db['internal_connection']['port']
    );
}