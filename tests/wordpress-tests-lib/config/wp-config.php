<?php

/**
 * Loads local environment variables from file.
 *
 * Use `vendor/bin/dump-env .env` > .env.php to generate this file
 */
@include __DIR__.'/.env.php';

/**
 * The base configuration for WordPress
 */

/**
 * Database credentials
 */
define('DB_NAME',       getenv('WP_DB_NAME'));
define('DB_USER',       getenv('WP_DB_USER'));
define('DB_PASSWORD',   getenv('WP_DB_PASSWORD'));
define('DB_HOST',       getenv('WP_DB_HOST') ?: 'localhost');
define('DB_CHARSET',    getenv('WP_DB_CHARSET') ?: 'utf8');
define('DB_COLLATE',    getenv('WP_DB_COLLATE') ?: '');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = getenv('WP_TABLE_PREFIX') ?: 'wp_';

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';Y7A_{ubbm=x=[.5]9^76[*/U;~nLmwKds>p<!6%-{w^Cgr&g_:|Vg+:VA3Uc);Q');
define('SECURE_AUTH_KEY',  '`N&*1CpSH%)u-:8JK6+K+y<$KaKg_xJ#];G&9Z!9O?7ba6d/6A/=e*$=v7z+ws~?');
define('LOGGED_IN_KEY',    'twZPVoa2nWy,~JlY~t3`3A^lSG!81Hw*[d@@k mwfKPArAjO`pwAD0}7bF291|$:');
define('NONCE_KEY',        '6mU}f*@!WK73?ac$`T!_pPZ$VP`Vyg_9?gN3lEaqD(2ur[_hM/ C=53};)RXcBad');
define('AUTH_SALT',        't_0F5&31KShdddZP*z3WTtUqW3j|6D7#2i>J}g:O{o%(w/S^0A* I(YG&i5(dhm~');
define('SECURE_AUTH_SALT', 'xPRx.^$hpaMN+hfY.1gggi5kalJl?oKT{YFX6Kt+nara/FsMp{sMyh+Cetey|]s7');
define('LOGGED_IN_SALT',   's|,ye$R#v:5X}HjI*Y/.D]V>Od@,E_,-OBbV:/Hi/p#@(b1 H5`nuB@:yK-)>l?B');
define('NONCE_SALT',       'g0Y0!F@1,9 D+DpMQzlGl+X$$IW.l.KtzWsL#>`z/fU*df2iRG)!gBf+$=,99b]|');


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', getenv('WP_DEBUG') ?: false);

/**
 * Updates and editor settings
 */
define('DISALLOW_FILE_EDIT', true); // disables plugin and theme editor
define('DISALLOW_FILE_MODS', true); // disables plugin and theme updates

// disables **automatic** core updates
define('AUTOMATIC_UPDATER_DISABLED', true);
define('WP_AUTO_UPDATE_CORE', false);
