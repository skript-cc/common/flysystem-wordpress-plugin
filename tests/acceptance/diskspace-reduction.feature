Feature: disk space reduction
    As a system operator
    I want uploads to be saved to an external file storage
    And removed from the local file system
    In order to limit the amount of disk space needed for running Wordpress
