Feature: file replication
    As a system operator
    I want uploads to be saved to an external file storage
    In order to save disk space
    And to make Wordpress run faster

Scenario Outline: upload a file
    As a content administrator
    Given I'm logged in to Wordpress
    When I upload a file to <location>
    Then the file should be uploaded like normal
    And the file should be written to the flysystem
    
    Examples:
        | location |
        | the media library |
        | a page |
        | a post |

Scenario Outline: request an uploaded file
    As a website visitor
    When I request a file that is uploaded to <location>
    Then the file should be served from the flysystem
    
    Examples:
        | location |
        | the media library |
        | a page |
        | a post |

Scenario Outline: upload an image
    As a content administrator
    Given I'm logged in to Wordpress
    When I upload an image to <location>
    Then the image should be uploaded like normal
    And the image should be written to the flysystem
    And all created subsizes of the image should be written to the flysystem
    
    Examples:
        | location |
        | the media library |
        | a page |
        | a post |

Scenario: edit an image in the media library
    As a content administrator
    Given I'm logged in to Wordpress
    When I edit an image in the media Library
    And save the image
    Then the image should be saved like normal
    And the image should be written to the flysystem
    And all image sizes which are changed should be written to the flysystem

Scenario: delete a file from the media library
    As a content administrator
    Given I'm logged in to Wordpress
    When I permanently delete a file from the media Library
    Then the file should be deleted from the file system
    And the file should be deleted from the flysystem

Scenario: delete an image from the media library
    As a content administrator
    Given I'm logged in to Wordpress
    When I permanently delete an image from the media Library
    Then the image should be deleted from the file system
    And the image should be deleted from the flysystem
    And all image subsizes should be deleted from the flysystem