<?php

/**
 * PHPUnit bootstrap file
 */
 
const PLUGIN_ROOT = __DIR__ . '/../';
const WP_TEST_LIB = __DIR__ . '/wordpress-tests-lib';

require_once PLUGIN_ROOT . '/vendor/autoload.php';
require_once WP_TEST_LIB . '/includes/functions.php';

/**
 * Manually load the plugin being tested.
 */
tests_add_filter('muplugins_loaded', function () { 
    require PLUGIN_ROOT . '/flysystem.php'; 
});

// Start up the WP testing environment.
require WP_TEST_LIB . '/includes/bootstrap.php';
