<?php

namespace Skript\Wpp\FlySystem;

/**
 * Plugin tests
 */
class PluginTest extends \WP_UnitTestCase
{
    public function testRun() {
        $this->assertTrue(run());
    }
    
    public function testActivationHookIsRegistered()
    {
        global $wp_filter;
        
        $activationHook = $wp_filter['activate_app/flysystem.php'];
        
        $this->assertTrue(isset($activationHook));
        $this->assertTrue($activationHook->has_filter(__NAMESPACE__.'\on_activate'));
    }
    
    public function testDeactivationHookIsRegistered()
    {
        global $wp_filter;
        
        $activationHook = $wp_filter['deactivate_app/flysystem.php'];
        
        $this->assertTrue(isset($activationHook));
        $this->assertTrue($activationHook->has_filter(__NAMESPACE__.'\on_deactivate'));
    }
}
